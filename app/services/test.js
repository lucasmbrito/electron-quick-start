const axios = require('axios');
const {app} = require('electron');
const parse = require('csv-parser');
const fs = require('fs');
const serviceCotista = require('./type-integration');
const URL_ATLAS = 'http://nt1155:803/AtlasPasWebApi/api/Exclusive/IncluirAlterarCotistaDistribuicao';

/*exports.main = () => {
  axios.get('http://localhost:8081/fundo').then( res => {
    app.quit();
    return res
  })
}*/

send = async (body, auth) => {
  await axios.post(URL_ATLAS, body, {
    headers: {
      'Authorization': auth,
      'Content-Type': 'application/json',
    }
  }).then(function (res) {
    console.log(res.data.Mensagem);
  }).catch(function (error) {
    console.log(error.response.data);
  });
}

sendCotista = async (cotistas, auth) => {
  for (let i = 0; i < cotistas.length; i++) {
    let res = await serviceCotista.distributorType(process.argv[3] || 'XP');
    res.Nome = cotistas[i].NOME;
    res.Apelido = cotistas[i].NOME;
    await send(res, auth);    
  }
}

exports.readFile = () => {
  const username = 'admin';
  const password = 'teste12345';
  const auth = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');
  const list = [];
  fs.createReadStream(process.argv[2], {encoding: 'utf8'})    
    .pipe(parse({separator: ';'}))
    .on('data', (row) => {
      list.push(row);
    })
    .on('end', async () => {
      const unique = await list.filter((item, i, arr) => arr.findIndex(t => t.NOME === item.NOME) === i);
      await sendCotista(unique, auth);
      console.log('PROCESSO EXECUTADO COM SUCESSO.');
      app.quit();
    });
}