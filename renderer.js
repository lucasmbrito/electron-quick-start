// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

function test(){
  global.sharedObject = {prop1: process.argv}
  var remote = require('electron').remote,
  arguments = remote.getGlobal('sharedObject').prop1;
  console.log(arguments);
  return arguments;
}
